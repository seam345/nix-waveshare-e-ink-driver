{ stdenv, pkgs }:
let

  nixpkgs = import <nixpkgs> {};

  inherit (nixpkgs) stdenv fetchurl which;

  actualHello = stdenv.mkDerivation {
    name = "bcm2835";

    src = fetchurl {
      url = "http://www.airspayce.com/mikem/bcm2835/bcm2835-1.60.tar.gz";
      sha256 = "09rdwmkkfly4hgq0rby2h95apg46xrrlf1wqj3111scmx3p7qgik";
    };

    buildPhase = ''
      ./configure
      sed -i 's/ @/ /' Makefile
      make
      cat Makefile
      make install prefix="$out"
      ls src
      ls
      ls -l
    '';

    installPhase = ''
      mkdir -p $out/include
      mkdir -p $out/lib
      ls -l
      echo ###################################################################
      cp -r src/* $out/include
      cp -r src/* $out/lib
      #exit 1
    '';

  };



  wiringPi = stdenv.mkDerivation {
    name = "wiringPi";

    src = fetchurl {
      url = "https://github.com/WiringPi/WiringPi/archive/refs/tags/2.61-1.tar.gz";
      sha256 = "04iy0fhcqr32721vjcva471ky7m8iasxgbrgc37rld515dn6rp5m";
    };
    buildInputs = [ which ];

    buildPhase = ''
     mkdir -p $out/include
     mkdir -p $out/lib
     cd wiringPi
     ## hmmm the bellow gpiolayout number seems to not matter ... odd
     #sed -i '752c static int  gpioLayout = 1 ;' wiringPi.c # Horrid hack as nixos /proc/cpuinfo doesnt have a hardwareline ( i think pi 4 came out to be 1 but could be 2)
     sed -i '752c static int  gpioLayout = 2 ;' wiringPi.c
     sed -i '959c char line[] = "Revision : c03111";' wiringPi.c
     sed -i '969c \\' wiringPi.c
     sed -i '970c \\' wiringPi.c
     sed -i '972c \\' wiringPi.c
     sed -i '973c \\' wiringPi.c
     sed -i '974c \\' wiringPi.c
     sed -i '976c \\' wiringPi.c
     cat wiringPi.c
     echo -n "wiringPi:   "     ;
     make
     #make install DESTDIR='../install/' PREFIX='/.' Q=' ' LDCONFIG=' '
     cp *.o ../devLib
     cp *.o $out/include
     cp *.o $out/lib
     cp *.h ../devLib
     cp *.h $out/include
     cp *.h $out/lib
     cp libwiringPi* $out/lib
     ln -sf $out/lib/libwiringPi* $out/lib/libwiringPi.so
     cd ../devLib
     echo -n "DevLib:     "     ;
     make DESTDIR="$out" PREFIX='./' Q=' '
     #make install DESTDIR='../install/' PREFIX='/.' Q=' ' LDCONFIG=' '
     cp *.o $out/include
     cp *.o $out/lib
     cp *.o ../gpio
     cp *.h $out/include
     cp *.h $out/lib
     cp *.h ../gpio
     cp libwiringPiDev* $out/lib
     ln -sf $out/lib/libwiringPiDev* $out/lib/libwiringPiDev.so
     cd ../gpio
     ls
     echo -n "gpio:       "     ;
     make  DESTDIR="$out" PREFIX='/.' Q=' '
     #make install DESTDIR='../install/' PREFIX='./' Q=' ' LDCONFIG=' '
     cp *.o $out/include
     cp *.o $out/lib
     cp *.h $out/include
     cp *.h $out/lib
     #cd ../examples
     #echo -n "Examples:   "    ; make
     #cd Gertboard
     #echo -n "Gertboard:  "    ; make
     #cd ../PiFace
     #echo -n "PiFace:     "    ; make
     #cd ../q2w
     #echo -n "Quick2Wire: "    ; make
     #cd ../PiGlow
     #echo -n "PiGlow:     "    ; make
     #cd ../scrollPhat
     #echo -n "scrollPhat: "    ; make
     cd ..
    '';


    installPhase = ''
      ls -l
      #cp -r wiringPi/* $out/include
      #cp -r wiringPi/* $out/lib
    '';

  };



in stdenv.mkDerivation {
    name = "hello-wrapper";

    src = fetchGit {
      url = "https://gitlab.com/seam345/nix-waveshare-e-ink-driver.git";
      ref = "master";
    };

    nativeBuildInputs = [ nixpkgs.unzip];


    buildInputs = [ actualHello  pkgs.jq ];

    buildPhase = ''
      ls
      ls
      cd lcd/
      mkdir bin
      #sed -i 's/USELIB = USE_WIRINGPI_LIB/#USELIB = USE_WIRINGPI_LIB/' Makefile
      #sed -i 's/$(LIB)//' Makefile
      cat Makefile
      make
    '';


    installPhase = ''
      mkdir -p $out
      ls
      cp -r "bin" $out
      cp -r "pic" $out
      cp main $out
    '';

  }
