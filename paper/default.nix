{ stdenv, pkgs }:
let

  nixpkgs = import <nixpkgs> {};

  inherit (nixpkgs) stdenv fetchurl which;

  bcm2835 = stdenv.mkDerivation {
    name = "bcm2835";

    src = fetchurl {
      url = "http://www.airspayce.com/mikem/bcm2835/bcm2835-1.60.tar.gz";
      sha256 = "09rdwmkkfly4hgq0rby2h95apg46xrrlf1wqj3111scmx3p7qgik";
    };

    buildPhase = ''
      ./configure
      sed -i 's/ @/ /' Makefile
      make
      cat Makefile
      make install prefix="$out"
    '';

    installPhase = ''
      mkdir -p $out/include
      mkdir -p $out/lib
      cp -r src/* $out/include
      cp -r src/* $out/lib
    '';

  };



in stdenv.mkDerivation {
  name = "hello-wrapper";

  src = fetchGit {
		url = "https://gitlab.com/seam345/nix-waveshare-e-ink-driver.git";
		ref = "master";
	};

  buildInputs = [ bcm2835 ];

  buildPhase = ''
    cd paper
    make RPI
  '';


  installPhase = ''
    mkdir -p $out/bin
    mkdir -p $out/pic
    mkdir -p $out/examples
    cp -r bin/* $out/bin
    cp -r pic/* $out/pic
    cp -r examples/* $out/examples
    cp ./epd $out/epd
    ls
  '';

}
